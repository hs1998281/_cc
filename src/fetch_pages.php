<?php
//sleep(2);
include("config.inc.php"); //include config file


//sanitize post value
$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);

//throw HTTP error if page number is not valid
if(!is_numeric($page_number)){
    header('HTTP/1.1 500 Invalid page number!');
    exit;
}

//get current starting point of records
$position = (($page_number-1) * $item_per_page);
//fetch records using page position and item per page. 
$results = $mysqli->prepare("SELECT id, name, message FROM paginate ORDER BY id DESC LIMIT ?, ?");

//bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
//for more info https://www.sanwebe.com/2013/03/basic-php-mysqli-usage
$results->bind_param("dd", $position, $item_per_page); 
$results->execute(); //Execute prepared Query
$results->bind_result($id, $name, $message); //bind variables to prepared statement

//output results from database
while($results->fetch()){ //fetch values
    echo 
    '<div class="panel">
        
        <h5>Terpopuler</h5>
        
        <div class="panel-body">
            <div>
                <a href="#" title="'.$name.'">
                    <figure class="pull-right">
                        <img src="https://siva.jsstatic.com/id/51747/images/logo/51747_logo_0_223286.jpg" style="display: inline;">
                    </figure>
                </a>								
            </div>
            <div>
                <a title="Lihat detil lowongan - Quality Assurance Officer">
                    <h2>'.$name.'</h2>
                </a>
            </div>
            <h4>
                <a>PT Permodalan Nasional Madani</a>
            </h4>
            <ul class="list-unstyled">
                <li>'.$message.'</li>																													
            </ul>
            <div>
                <div>
                    <i class="fa fa-calendar"></i>
                    <span class="text">12-12-2018</span>&nbsp;
                    <a href="#">Perlihatkan lebih banyak</a>
                </div>
            </div>
        </div>
    </div>    
    '; 
}